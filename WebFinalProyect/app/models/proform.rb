class Proform < ApplicationRecord
    belongs_to :quotation
    has_one :payment
end