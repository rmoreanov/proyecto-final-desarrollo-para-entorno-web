class Proyect < ApplicationRecord
    has_many :department, :dependent => :destroy

    validates :name, presence: {message: "presence"}, uniqueness: {case_sensitive: false, message: "uniqueness"}
    validates :base_price_m2, presence: {message: "presence"}, numericality: {message: "numericality"}
    validate :map_start_with

    def map_start_with
        if google_maps_embed_url.present?
            if !google_maps_embed_url.start_with?("https://www.google.com/maps/embed", "https://maps.google.com/maps")
                errors.add(:google_maps_embed_url, "start_with")
            end
        end
    end
end