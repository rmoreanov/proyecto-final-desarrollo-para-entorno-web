class Departmentvideo < ApplicationRecord
    belongs_to :department

    validates :name, presence: {message: "presence"}, uniqueness: {case_sensitive: false, scope: :department_id, message: "uniqueness"}
    validates :youtube_url, presence: {message: "presence"}, uniqueness: {case_sensitive: false, scope: :department_id, message: "uniqueness"}
    validate :youtube_start_with

    def youtube_start_with
        if youtube_url.present?
            if !youtube_url.start_with?("https://www.youtube.com/watch?v=")
                errors.add(:youtube_url, "start_with")
            end
        end
    end
end