class Departmentimage < ApplicationRecord
    belongs_to :department

    validates :name, presence: {message: "presence"}, uniqueness: {case_sensitive: false, scope: :department_id, message: "uniqueness"}
end