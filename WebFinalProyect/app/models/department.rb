class Department < ApplicationRecord
    belongs_to :proyect
    has_many :departmentimage, :dependent => :destroy
    has_many :departmentvideo, :dependent => :destroy
    has_many :quotation
    has_one :payment
    
    validates :category, presence: {message: "presence"}, inclusion: {in: 1..6, message: "inclusion"}
    validates :number, presence: {message: "presence"}, uniqueness: {case_sensitive: false, scope: :proyect_id, message: "uniqueness"}
    validates :floor, presence: {message: "presence"}, numericality: {message: "numericality"}
    validates :area_m2, presence: {message: "presence"}, numericality: {message: "numericality"}
    validates :bathrooms, presence: {message: "presence"}, numericality: {message: "numericality"}
    validates :bedrooms, presence: {message: "presence"}, numericality: {message: "numericality"}
end