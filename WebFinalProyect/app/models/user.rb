class User < ApplicationRecord
    has_many :quotation

    validates :dni, presence: {message: "presence"}, uniqueness: {case_sensitive: false, message: "uniqueness"}, length: {minimum: 8, maximum: 12, message: "length"}
    validates :names, presence: {message: "presence"}
    validates :lastnames, presence: {message: "presence"}
    validates :profile, presence: {message: "presence"}, inclusion: {in: 2..3, message: "inclusion"}, numericality: {message: "numericality"}
    validates :password_hashed, presence: {message: "presence"}
    validate :password_length

    def password_length
        if password_hashed.length<5
            errors.add(:password_hashed, "length")
        else
            self.password_hashed=BCrypt::Password.create(password_hashed)
        end
    end
end