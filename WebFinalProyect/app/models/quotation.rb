class Quotation < ApplicationRecord
    belongs_to :department
    belongs_to :user
    has_one :proform

    validates :dni, presence: {message: "presence"}, length: {minimum: 8, maximum: 12, message: "length"}
    validates :names, presence: {message: "presence"}
    validates :lastnames, presence: {message: "presence"}
    validates :email, presence: {message: "presence"}
    validates :telephone, presence: {message: "presence"}, length: {minimum: 7, maximum: 12, message: "length"}
    
end