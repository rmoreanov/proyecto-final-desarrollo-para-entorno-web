class ProformsController < ApplicationController
    def index
        allow_access([3])
        @proforms = Proform.select { |proform| proform.quotation.user.id == session[:user_id] }
    end
    def create
        allow_access([3])
        @quotation = Quotation.find(params[:quotation_id])
        @cost=@quotation.department.proyect.base_price_m2*@quotation.department.area_m2
        @discount_rate=10;
        if @quotation.department.floor>=1 and @quotation.department.floor<=10
            @discount_rate=@quotation.department.floor;
        end
        @discount=@cost*@discount_rate/100
        @proform = Proform.new({:dni=>@quotation.dni,:names=>@quotation.names,:lastnames=>@quotation.lastnames,:email=>@quotation.email,:telephone=>@quotation.telephone,
            :proyect=>@quotation.department.proyect.name,:department=>@quotation.department.number,:floor=>@quotation.department.floor,
            :cost=>@cost, :discount=>@discount, :total=>@cost-@discount, :quotation_id=>@quotation.id
        })
        if @proform.save
            render json: {:url=>url_for(controller: 'quotations', action: 'appointments')}
        end
    end
    def show
        @proform=Proform.find(params[:proform_id])
    end
end