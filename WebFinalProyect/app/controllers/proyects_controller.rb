class ProyectsController < ApplicationController
    before_action :set_proyect, only: [:show, :edit, :update, :destroy]
    def index
        allow_access([1])
        @proyects = Proyect.all
    end
    def new
        allow_access([1])
        @proyect = Proyect.new
    end
    def create
        allow_access([1])
        @proyect = Proyect.new(proyect_params)
        if @proyect.save
            FileUtils.mkdir_p("public/assets/img/proyects/"+@proyect[:id].to_s+"/departments")
            render json: {:success=>true, :url=>url_for(controller: 'proyects', action: 'index')}
        else
            render json: @proyect.errors.messages
        end
    end
    def editcoverpage
        allow_access([1])
        set_proyect()
    end
    def createcoverpage
        allow_access([1])
        tmp = params[:picture].tempfile
        extname = File.extname(params[:picture].original_filename)
        if extname == ".jpg"
            file = File.join("public/assets/img/proyects/"+params[:proyect_id], "coverpage"+extname)
            FileUtils.cp tmp.path, file
            render json: {:success=>true}
        else
            render json: {:image=>["extname"], :url=>url_for(controller: 'proyects', action: 'uploadimagecoverpage')}
        end
    end
    def show
        @departments = Department.select{|department| department.proyect_id == params[:proyect_id].to_i and department.state==true}
        @images={}
        for i in 0...@departments.size
            @images[@departments[i].id]=[]
            for j in 0...@departments[i].departmentimage.size
                @images[@departments[i].id] << @departments[i].departmentimage[j].id
            end
        end
    end
    def edit
        allow_access([1])
    end
    def update
        allow_access([1])
        if @proyect.update(proyect_params)
            render json: {:success=>true}
        else
            render json: @proyect.errors.messages
        end
    end
    def destroy
        allow_access([1])
        @proyect.destroy
        FileUtils.rm_rf("public/assets/img/proyects/"+@proyect[:id].to_s)
        redirect_to :controller=>'proyects',:action=>'index'
    end

    private
    def set_proyect
        @proyect = Proyect.find(params[:proyect_id])
    end
    def proyect_params
        params.require(:proyect).permit(:name, :address, :base_price_m2, :google_maps_embed_url)
    end
end