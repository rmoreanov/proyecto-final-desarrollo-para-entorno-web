class UsersController < ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]

    # 0 Normal
    # 1 Administrador
    # 2 Gerente
    # 3 Asesor

    def index
        allow_access([1])
        @users = User.where.not(profile: 1)
    end
    def new
        allow_access([1])
        @user = User.new
    end
    def create
        allow_access([1])
        @user_params=user_params
        @user_params[:password_hashed]=@user_params.delete(:password)
        @user = User.new(@user_params)
        if @user.save
            render json: {:success=>true, :url=>url_for(controller: 'users', action: 'index')}
        else
            render json: @user.errors.messages
        end
    end
    def login
        allow_access([0])
    end
    def access
        allow_access([0])
        @user = User.where(:dni => params[:dni]).first
        if @user and BCrypt::Password.new(@user[:password_hashed]) == params[:password]
            session[:user_id] = @user[:id]
            render json: {:success=>true, :url=>url_for(controller: 'home', action: 'index')}
        else
            render json: {:login=>["error"]}
        end
    end
    def logout
        allow_access([1,2,3])
        session.delete(:user_id)
        redirect_to :controller=>'home',:action=>'index'
    end
    def show
        
    end
    def edit
        
    end
    def update
        allow_access([1])
        @user_params=user_params
        @user_params[:password_hashed]=@user_params.delete(:password)
        if @user.update(@user_params)
            render json: {:success=>true, :url=>url_for(controller: 'users', action: 'index')}
        else
            render json: @user.errors.messages
        end
    end
    def destroy
        allow_access([1])
        @user.destroy
        redirect_to :controller=>'users',:action=>'index'
    end

    private
    def set_user
      @user = User.find(params[:user_id])
    end
    def user_params
      params.require(:user).permit(:dni, :names, :lastnames, :profile, :password)
    end
    def validate_password(password)
        @error=[]
        if password==""
            @error.push("presence")
        end
        if password.length<=4
            @error.push("length")
        end
        return @error
    end
end