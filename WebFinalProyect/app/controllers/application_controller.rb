class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :validate_user

  $title="J&J"

  def allow_access(profile)
    if profile.exclude?(@profile)
      redirect_to :controller=>'home',:action=>'index'
      return false
    end
  end

  private
  def validate_user
    if session[:user_id]
      @user = User.find(session[:user_id])
      if @user
        @profile=@user[:profile]
      else
        session.delete(:user_id)
        redirect_to :controller=>'home',:action=>'index'
      end
    else
      @profile=0
    end
  end
end
