class QuotationsController < ApplicationController
    def index
        allow_access([3])
        @proyects = Proyect.all
        @quotations = Quotation.where(:user_id => session[:user_id])
        @proforms = Proform.all
        @quotationDepartment=[]
        for i in 0...@proforms.size
            @quotationDepartment << @proforms[i].quotation.department_id
        end
    end
    def find
        allow_access([3])
        @proyect_id=params[:proyect_id]
        @proyects = Proyect.all
        @quotations = Quotation.where(:user_id => session[:user_id])
        @proforms = Proform.all
        @quotationDepartment=[]
        for i in 0...@proforms.size
            @quotationDepartment << @proforms[i].quotation.department_id
        end
        render :action=>:index
    end
    def new
        @department = Department.find(params[:department_id])
        @quotation=Quotation.new
    end
    def create
        @quotation_params=quotation_params
        @quotation_params[:status]=0
        @quotation_params[:appointment]=nil
        @advisers=User.where(:profile => 3)
        if @advisers.any?
            @adviser_min=@advisers[0]
            for i in 1...@advisers.size
                if @advisers[i].quotation.size<@adviser_min.quotation.size
                    @adviser_min=@advisers[i]
                end
            end
            @quotation_params[:user_id]=@adviser_min.id
            @quotation = Quotation.new(@quotation_params)
            if @quotation.save
                render json: {:success=>true, :url=>url_for(controller: 'home', action: 'waitus')}
            else
                render json: @quotation.errors.messages
            end
        else
            render json: {:advisers=>["presence"]}
        end
    end
    def updateappointment
        allow_access([3])
        set_quotation()
        if params.has_key?(:notinterested)
            @quotation.update_attribute(:status,2)
            render json: {:success=>true, :url=>url_for(controller: 'quotations', action: 'index')}
        else
            @validatedate = validatedate(params.require(:quotation)[:date])
            if @validatedate.empty?
                if @quotation.appointment
                    @quotation.update_attribute(:appointment, params.require(:quotation)[:date])
                    render json: {:success=>true, :url=>url_for(controller: 'quotations', action: 'appointments')}
                else
                    @quotation.update_attribute(:status,1)
                    @quotation.update_attribute(:appointment, params.require(:quotation)[:date])
                    render json: {:success=>true, :url=>url_for(controller: 'quotations', action: 'index')}
                end
            else
                render json: @validatedate
            end
        end
    end
    def showquotation
        
    end
    def appointments
        allow_access([3])
        @quotations = Quotation.where(:user_id => session[:user_id], :status => 1)
    end
    def editappointment
        allow_access([3])
        set_quotation()
    end

    private
    def set_quotation
        @quotation = Quotation.find(params[:quotation_id])
    end
    def quotation_params
        params.require(:quotation).permit(:dni, :names, :lastnames, :email, :telephone, :department_id)
    end
    def validatedate(date)
        @errors={}
        if date.empty?
            @errors[:date]=["presence"]
        end
        return @errors
    end
end