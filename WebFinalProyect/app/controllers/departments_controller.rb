class DepartmentsController < ApplicationController
    before_action :set_department, only: [:show, :edit, :update, :destroy]
    def index
        allow_access([1])
        set_proyect()
        @departments = Department.where(:proyect_id => params[:proyect_id])
    end
    def new
        allow_access([1])
        set_proyect()
        @department=Department.new
    end
    def create
        allow_access([1])
        @department_params = department_params
        @department_params[:state]=true
        @department = Department.new(@department_params)
        if @department.save
            FileUtils.mkdir_p("public/assets/img/proyects/"+params[:proyect_id].to_s+"/departments/"+@department[:id].to_s)
            render json: {:success=>true, :url=>url_for(controller: 'departments', action: 'index')}
        else
            render json: @department.errors.messages
        end
    end
    def show
        @departmentimages = Departmentimage.where(:department_id => params[:department_id])
        @departmentvideos = Departmentvideo.where(:department_id => params[:department_id])
    end
    def edit
        allow_access([1])
        set_proyect()
    end
    def update
        allow_access([1])
        @department_params = department_params
        @department_params[:state]=true
        if @department.update(@department_params)
            render json: {:success=>true}
        else
            print @department.errors.messages
            render json: @department.errors.messages
        end
    end
    def destroy
        allow_access([1])
        @department.destroy
        FileUtils.rm_rf("public/assets/img/proyects/"+params[:proyect_id]+"/departments/"+@department[:id].to_s)
        redirect_to :controller=>'departments',:action=>'index'
    end

    def images
        set_proyect()
        set_department()
        @departmentimages = Departmentimage.where(:department_id => params[:department_id])
    end
    def showimage

    end
    def newimage
        set_proyect()
        set_department()
        @departmentimage = Departmentimage.new
    end
    def createimage
        tmp = image_params[:file].tempfile
        extname = File.extname(image_params[:file].original_filename)
        if extname == ".jpg"
            @departmentimage = Departmentimage.new(image_params.except(:file))
            if @departmentimage.save
                file = File.join("public/assets/img/proyects/"+params[:proyect_id]+"/departments/"+params[:department_id], @departmentimage[:id].to_s+extname)
                FileUtils.cp tmp.path, file
                render json: {:success=>true, :url=>url_for(controller: 'departments', action: 'images')}
            else
                render json: @departmentimage.errors.messages
            end
        else
            render json: {:image=>["extname"], :url=>url_for(controller: 'departments', action: 'newimage')}
        end
    end
    def editimage
        set_proyect()
        set_department()
        @departmentimage = Departmentimage.find(params[:image_id])
    end
    def updateimage
        set_department()
        set_image()
        if image_params[:file]
            tmp = image_params[:file].tempfile
            extname = File.extname(image_params[:file].original_filename)
            if extname == ".jpg"
                if @departmentimage.update(image_params.except(:file))
                    tmp = image_params[:file].tempfile
                    extname = File.extname(image_params[:file].original_filename)
                    file = File.join("public/assets/img/proyects/"+params[:proyect_id].to_s+"/departments/"+params[:department_id], @departmentimage[:id].to_s+extname)
                    FileUtils.cp tmp.path, file
                    render json: {:success=>true, :url=>url_for(controller: 'departments', action: 'images')}
                else
                    render json: @departmentimage.errors.messages
                end
            else
                render json: {:image=>["extname"], :url=>url_for(controller: 'departments', action: 'editimage')}
            end
        else
            if @departmentimage.update(image_params.except(:file))
                render json: {:success=>true, :url=>url_for(controller: 'departments', action: 'images')}
            else
                render json: @departmentimage.errors.messages
            end
        end        
    end
    def destroyimage
        set_image()
        @departmentimage.destroy
        FileUtils.rm_rf("public/assets/img/proyects/"+params[:proyect_id].to_s+"/departments/"+params[:department_id].to_s+"/"+@departmentimage.id.to_s+".jpg")
        redirect_to :controller=>'departments',:action=>'images'
    end
    
    def videos
        set_proyect()
        set_department()
        @departmentvideos = Departmentvideo.where(:department_id => params[:department_id])
    end
    def showvideo

    end
    def newvideo
        set_proyect()
        set_department()
        @departmentvideo = Departmentvideo.new
    end
    def createvideo
        @departmentvideo = Departmentvideo.new(video_params)
        if @departmentvideo.save
            render json: {:success=>true, :url=>url_for(controller: 'departments', action: 'videos')}
        else
            render json: @departmentvideo.errors.messages
        end
    end
    def editvideo
        set_proyect()
        set_department()
        @departmentvideo = Departmentvideo.find(params[:video_id])
    end
    def updatevideo
        set_department()
        set_video()
        if @departmentvideo.update(video_params)
            render json: {:success=>true, :url=>url_for(controller: 'departments', action: 'videos')}
        else
            render json: @departmentvideo.errors.messages
        end
    end
    def destroyvideo
        set_video()
        @departmentvideo.destroy
        redirect_to :controller=>'departments',:action=>'videos'
    end

    def separate

    end
    
    

    private
    def set_department
        @department = Department.find(params[:department_id])
    end
    def set_proyect
        @proyect = Proyect.find(params[:proyect_id])
    end
    def set_image
        @departmentimage = Departmentimage.find(params[:image_id])
    end
    def set_video
        @departmentvideo = Departmentvideo.find(params[:video_id])
    end
    def department_params
        params.require(:department).permit(:category, :number, :floor, :area_m2, :bathrooms, :bedrooms, :description, :proyect_id)
    end
    def image_params
        params.require(:departmentimage).permit(:name, :description, :file, :department_id)
    end
    def video_params
        params.require(:departmentvideo).permit(:name, :youtube_url, :description, :department_id)
    end
end