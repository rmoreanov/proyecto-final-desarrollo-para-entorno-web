class ReportsController < ApplicationController
    def index
        allow_access([2])
        @proyects = Proyect.all
        @departmentpayment={}
        @payments = Payment.all
        @payments.each do |payment|
            @departmentpayment[payment.proform.quotation.department_id]=payment
        end
    end
    def find
        allow_access([2])
        @proyect_id=params[:proyect_id]
        @proyects = Proyect.all
        @departmentpayment={}
        @payments = Payment.all
        @payments.each do |payment|
            @departmentpayment[payment.proform.quotation.department_id]=payment
        end
        render :action=>:index
    end
end