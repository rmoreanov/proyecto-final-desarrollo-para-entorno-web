class PaymentsController < ApplicationController
    def index
        allow_access([3])
        @payments = Payment.select { |payment| payment.proform.quotation.user.id == session[:user_id] }
	end
    def separate
        allow_access([3])
        @proform = Proform.find(params[:proform_id])
        @payment = Payment.new;
    end
    def newupdate
        allow_access([3])
        @proform = Proform.find(params[:proform_id])
        @payment = Payment.new
    end
    def create
        allow_access([3])
        @proform = Proform.find(payment_params[:proform_id])
        if params.has_key?(:canceled)
            @payment_params=payment_params
            @validated_operation=validated_operation(payment_params[:operation], payment_params[:operation_amount])
            if @validated_operation.empty?
                @proform.quotation.department.update_attribute(:state,false);
                if @proform.payment
                    @proform.payment.update_attribute(:operation,payment_params[:operation])
                    @proform.payment.update_attribute(:operation_amount,payment_params[:operation_amount])
                else
                    @payment = Payment.new(@payment_params)
                    @payment.save
                end
                render json: {:success=>true, :url=>url_for(controller: 'payments', action: 'index')}
            else
                render json: @validated_operation
            end
        else
            if payment_params[:file]
                tmp = payment_params[:file].tempfile
                extname = File.extname(payment_params[:file].original_filename)
                if extname == ".jpg"
                    @payment_params=payment_params.except(:file)
                    @validated_voucher=validated_voucher(payment_params[:voucher], payment_params[:voucher_amount])
                    if @validated_voucher.empty?
                        @payment = Payment.new(@payment_params)
                        @payment.save
                        @proform.quotation.department.update_attribute(:state,false);
                        file = File.join("public/assets/img/vouchers/", payment_params[:proform_id].to_s+extname)
                        FileUtils.cp tmp.path, file
                        render json: {:success=>true, :url=>url_for(controller: 'payments', action: 'index')}
                    else
                        render json: @validated_voucher
                    end
                else
                    render json: {:image=>["extname"], :url=>url_for(controller: 'payments', action: 'separate', proform_id: payment_params[:proform_id])}
                end
            end 
        end
    end
    def show

    end
    def update
        
    end

    private
    def payment_params
        params.require(:payment).permit(:voucher, :voucher_amount, :operation, :operation_amount, :file, :proform_id)
    end
    def validated_voucher(voucher, voucher_amount)
        @errors={}
        if Payment.where(:voucher=>voucher.downcase).any?
            @errors[:voucher]=["uniqueness"]
        end
        if voucher.empty?
            @errors[:voucher]=["presence"]
        end
        if voucher_amount.to_f<15000
            @errors[:voucher_amount]=["greater_than"]
        end
        if voucher_amount.empty?
            @errors[:voucher_amount]=["presence"]
        end
        return @errors
    end
    def validated_operation(operation, operation_amount)
        @proform=Proform.find(payment_params[:proform_id])
        @errors={}
        if Payment.select { |payment| payment.operation.to_s.downcase==operation.downcase or payment.voucher.to_s.downcase==operation.downcase }.any?
            @errors[:operation]=["uniqueness"]
        end
        if operation.empty?
            @errors[:operation]=["presence"]
        end
        if @proform.payment
            if operation_amount.to_f!=@proform.total.to_f-@proform.payment.voucher_amount.to_f
                @errors[:operation_amount]=["same_than"]
            end
        else
            if operation_amount.to_f!=@proform.total.to_f
                @errors[:operation_amount]=["same_than"]
            end
        end
        if operation_amount.empty?
            @errors[:operation_amount]=["presence"]
        end
        return @errors
    end
end