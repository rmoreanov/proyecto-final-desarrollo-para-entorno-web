Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #Home
  root 'home#index'
  get 'esperanos', to: 'home#waitus'
  get "buscar", to: 'home#find'

  #Login
  get 'login', to: 'users#login'
  post 'login', to: 'users#access'

  #Users
  post 'users/logout'
  get 'usuarios', to: 'users#index', as: 'users'
  post 'usuarios', to: 'users#create'
  get 'usuarios/registrar', to: 'users#new'
  get 'usuarios/:user_id/editar', to: 'users#edit'
  get 'usuarios/:user_id', to: 'users#index', as: 'user'
  patch 'usuarios/:user_id', to: 'users#update'
  put 'usuarios/:user_id', to: 'users#update'
  delete 'usuarios/:user_id', to: 'users#destroy'

  #Proyects
  get 'proyectos', to: 'proyects#index', as: 'proyects'
  post 'proyectos', to: 'proyects#create'
  get 'proyectos/registrar', to: 'proyects#new'
  get 'proyectos/:proyect_id/editar', to: 'proyects#edit'
  get 'proyectos/:proyect_id/portada', to: 'proyects#editcoverpage'
  post 'proyectos/:proyect_id/portada', to: 'proyects#createcoverpage'
  get 'proyectos/:proyect_id', to: 'proyects#show', as: 'proyect'
  patch 'proyectos/:proyect_id', to: 'proyects#update'
  put 'proyectos/:proyect_id', to: 'proyects#update'
  delete 'proyectos/:proyect_id', to: 'proyects#destroy'

  #Departments
  get 'proyectos/:proyect_id/departamentos', to: 'departments#index', as: 'departments'
  post 'proyectos/:proyect_id/departamentos', to: 'departments#create'
  get 'proyectos/:proyect_id/departamentos/registrar', to: 'departments#new'
  get 'proyectos/:proyect_id/departamentos/:department_id/editar', to: 'departments#edit'
  get 'proyectos/:proyect_id/departamentos/:department_id', to: 'departments#show', as: 'department'
  patch 'proyectos/:proyect_id/departamentos/:department_id', to: 'departments#update'
  put 'proyectos/:proyect_id/departamentos/:department_id', to: 'departments#update'
  delete 'proyectos/:proyect_id/departmentos/:department_id', to: 'departments#destroy'
  #   Images
  get 'proyectos/:proyect_id/departamentos/:department_id/imagenes', to: 'departments#images', as: 'departmentimages'
  post 'proyectos/:proyect_id/departamentos/:department_id/imagenes', to: 'departments#createimage'
  get 'proyectos/:proyect_id/departamentos/:department_id/imagenes/subir', to: 'departments#newimage'
  get 'proyectos/:proyect_id/departamentos/:department_id/imagenes/:image_id/editar', to: 'departments#editimage'
  get 'proyectos/:proyect_id/departamentos/:department_id/imagenes/:image_id', to: 'departments#showimage', as: 'departmentimage'
  patch 'proyectos/:proyect_id/departamentos/:department_id/imagenes/:image_id', to: 'departments#updateimage'
  put 'proyectos/:proyect_id/departamentos/:department_id/imagenes/:image_id', to: 'departments#updateimage'
  delete 'proyectos/:proyect_id/departamentos/:department_id/imagenes/:image_id', to: 'departments#destroyimage'
  #   Videos
  get 'proyectos/:proyect_id/departamentos/:department_id/videos', to: 'departments#videos', as: 'departmentvideos'
  post 'proyectos/:proyect_id/departamentos/:department_id/videos', to: 'departments#createvideo'
  get 'proyectos/:proyect_id/departamentos/:department_id/videos/subir', to: 'departments#newvideo'
  get 'proyectos/:proyect_id/departamentos/:department_id/videos/:video_id/editar', to: 'departments#editvideo'
  get 'proyectos/:proyect_id/departamentos/:department_id/videos/:video_id', to: 'departments#showvideo', as: 'departmentvideo'
  patch 'proyectos/:proyect_id/departamentos/:department_id/videos/:video_id', to: 'departments#updatevideo'
  put 'proyectos/:proyect_id/departamentos/:department_id/videos/:video_id', to: 'departments#updatevideo'
  delete 'proyectos/:proyect_id/departamentos/:department_id/videos/:video_id', to: 'departments#destroyvideo'

#Quotations
  get 'cotizaciones', to: 'quotations#index'
  get 'cotizaciones/buscar', to: 'quotations#find'
  get 'proyectos/:proyect_id/departamentos/:department_id/cotizar', to: 'quotations#new', as: 'quotations'
  post 'proyectos/:proyect_id/departamentos/:department_id/cotizar', to: 'quotations#create'
  get 'cotizaciones/:quotation_id/citar', to: 'quotations#editappointment'
  get 'cotizaciones/:quotation_id', to: 'quotations#showquotation', as: 'quotation'
  patch 'cotizaciones/:quotation_id', to: 'quotations#updateappointment'
  put 'cotizaciones/:quotation_id', to: 'quotations#updateappointment'
  get 'citas', to: 'quotations#appointments'
  get 'citas/:quotation_id/editar', to: 'quotations#editappointment'

#Proforms
  post 'crearproforma/:quotation_id', to: 'proforms#create'
  get 'proformas/:proform_id', to:'proforms#show'
  get 'proformas', to: 'proforms#index'

#Payments
  get 'pagos', to: 'payments#index'
  get 'proformas/:proform_id/separar', to:'payments#separate'
  get 'proformas/:proform_id/pagar', to:'payments#newupdate'
  post 'pagos', to: 'payments#create', as: 'payments'

#Reports
  get 'reportes', to: 'reports#index'
  get "reportes/buscar", to: 'reports#find'

  get '/*id', to: 'home#index'
end