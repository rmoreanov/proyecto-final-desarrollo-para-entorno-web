class CreateDepartmentvideos < ActiveRecord::Migration[5.1]
    def change
        create_table :departmentvideos do |t|
            t.string :name
            t.string :youtube_url
            t.string :description
            t.references :department, foreign_key: true

            t.timestamps
        end
    end
end