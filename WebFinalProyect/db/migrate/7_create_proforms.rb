class CreateProforms < ActiveRecord::Migration[5.1]
    def change
        create_table :proforms do |t|
            t.string :dni
            t.string :names
            t.string :lastnames
            t.string :email
            t.integer :telephone
            t.string :proyect
            t.string :department
            t.integer :floor
            t.decimal :cost
            t.decimal :discount
            t.decimal :total
            
            t.references :quotation, foreign_key: true

            t.timestamps
        end
    end
end