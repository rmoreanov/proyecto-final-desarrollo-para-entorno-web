class CreateProyects < ActiveRecord::Migration[5.1]
    def change
        create_table :proyects do |t|
            t.string :name
            t.decimal :base_price_m2
            t.string :address
            t.string :google_maps_embed_url

            t.timestamps
        end
    end
end