class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :dni
      t.string :names
      t.string :lastnames
      t.integer :profile
      t.string :password_hashed

      t.timestamps
    end
  end
end