class CreateQuotations < ActiveRecord::Migration[5.1]
    def change
        create_table :quotations do |t|
            t.string :dni
            t.string :names
            t.string :lastnames
            t.string :email
            t.integer :telephone
            t.integer :status
            t.datetime :appointment
            t.boolean :proform
            
            t.references :department, foreign_key: true
            t.references :user, foreign_key: true

            t.timestamps
        end
    end
end