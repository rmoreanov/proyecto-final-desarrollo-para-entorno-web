class CreateDepartments < ActiveRecord::Migration[5.1]
    def change
        create_table :departments do |t|
            t.integer :category
            t.string :number
            t.integer :floor
            t.decimal :area_m2
            t.integer :bathrooms
            t.integer :bedrooms
            t.string :description
            t.boolean :state
            t.references :proyect, foreign_key: true

            t.timestamps
        end
    end
end