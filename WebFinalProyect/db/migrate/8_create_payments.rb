class CreatePayments < ActiveRecord::Migration[5.1]
    def change
        create_table :payments do |t|
            t.string :voucher
            t.decimal :voucher_amount
            t.string :operation
            t.decimal :operation_amount

            t.references :proform, foreign_key: true
            t.timestamps
        end
    end
end