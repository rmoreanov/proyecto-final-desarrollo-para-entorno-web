# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 8) do

  create_table "departmentimages", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_images_on_department_id"
  end

  create_table "departments", force: :cascade do |t|
    t.integer "category"
    t.string "number"
    t.integer "floor"
    t.decimal "area_m2"
    t.integer "bathrooms"
    t.integer "bedrooms"
    t.string "description"
    t.integer "proyect_id"
    t.boolean "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["proyect_id"], name: "index_departments_on_proyect_id"
  end

  create_table "departmentvideos", force: :cascade do |t|
    t.string "name"
    t.string "youtube_url"
    t.string "description"
    t.integer "department_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_departmentvideos_on_department_id"
  end

  create_table "payments", force: :cascade do |t|
    t.string "voucher"
    t.decimal "voucher_amount"
    t.string "operation"
    t.decimal "operation_amount"
    t.integer "proform_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["proform_id"], name: "index_payments_on_proform_id"
  end

  create_table "proforms", force: :cascade do |t|
    t.string "dni"
    t.string "names"
    t.string "lastnames"
    t.string "email"
    t.integer "telephone"
    t.string "proyect"
    t.string "department"
    t.integer "floor"
    t.decimal "cost"
    t.decimal "discount"
    t.decimal "total"
    t.integer "quotation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["quotation_id"], name: "index_proforms_on_quotation_id"
  end

  create_table "proyects", force: :cascade do |t|
    t.string "name"
    t.decimal "base_price_m2"
    t.string "address"
    t.string "google_maps_embed_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quotations", force: :cascade do |t|
    t.string "dni"
    t.string "names"
    t.string "lastnames"
    t.string "email"
    t.integer "telephone"
    t.integer "status"
    t.datetime "appointment"
    t.integer "department_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_quotations_on_department_id"
    t.index ["user_id"], name: "index_quotations_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "dni"
    t.string "names"
    t.string "lastnames"
    t.integer "profile"
    t.string "password_hashed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
