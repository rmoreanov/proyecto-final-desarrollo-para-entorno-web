## PARA ACCEDER COMO USUARIO

http://localhost:3000/login

Las cuentas habilitadas son:

1. DNI: 99999999    PASSWORD: admin
2. DNI: 77777777    PASSWORD: admin

---

**UNIVERSIDAD PERUANA DE CIENCIAS APLICADAS**

**Curso:**          Desarrollo para el Entorno Web

**Sección:**        C31A

**Integrantes:**    Grupo 02

    Rolando Vidal Moreano Vargas			U20181E060

    Manuel Vilchez Maticorena 			    U20181D163

    Moises Daniel Chang Armas			    U201514128

    Carlos Ayrton Vera Juscamayta			U20181b714
    
**Coordinador:**

    Rolando Vidal Moreano Vargas	rolandomoreano@gmail.com

**LIMA – PERÚ**

**2019**